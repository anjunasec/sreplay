#!/bin/bash

# Copyright (C) 2005-2013  Amos Waterland <apw@debian.org>
#
# This file is part of sreplay.
#
# The sreplay program is free software; you can redistribute it
# and/or modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# The sreplay program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with the sreplay program; if not, write to the
# Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.

NAME=strace-diff
VERSION=0.1.1

function help()
{
    echo -e "\`$NAME' calculates the diff between sreplay and reality\n" \
	    "\n"                                                             \
	    "Usage: $NAME PROGRAM\n"                                         \
	    " -?, --help     Show this help statement.\n"                    \
	    "     --version  Show version statement.\n"                      \
	    "\n"                                                             \
	    "Examples: $NAME ls\n";
}

function histodiff()
{
    local program=$1;

    test -x ./sreplay || return 1;

    strace $program 1> /dev/null 2> good.log;
    strace -c $program 1> /dev/null 2> good.histo;
    strace -c ./sreplay $(cat good.log) 1> /dev/null 2> bad.histo;

    diff -u good.histo bad.histo > $program.strace.diff;

    more $program.strace.diff;

    rm good.log good.histo bad.histo $program.strace.diff;

    return 0;
}

function tracediff()
{
    local program=$1;

    strace $program 1> /dev/null 2> good.log;
    strace ./sreplay $(cat good.log) 1> /dev/null 2> bad.log;

    ./sreplay -n $(cat good.log) > good.parse;
    ./sreplay -n $(cat bad.log) > bad.parse;

    diff -u good.parse bad.parse > $program.strace.diff;

    rm good.log bad.log good.parse bad.parse;

    less $program.strace.diff;

    return 0;
}

function main()
{
    local program;
    local histogram;

    while [ $# -gt 0 ]; do
        case $1 in
        -\? | --help)
            help;
	    return 1;
            ;;
        --version)
            echo "$NAME $VERSION";
            return 1;
            ;;
        -c)
            histogram=1;
	    shift;
            ;;
        *)
	    program="$program $1";
	    shift;
            ;;
        esac
    done

    [ -n "$program" ] || {
	help;
	return 1;
    }

    if [ $histogram -eq 1 ]; then
	histodiff "$program" || { 
	    echo "$NAME: bailing out because of errors";
	    return 1;
	}
    else
	tracediff "$program" || { 
	    echo "$NAME: bailing out because of errors";
	    return 1;
	}
    fi

    return 0;
}

main "$@";
exit $?;
