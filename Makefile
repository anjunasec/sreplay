# Copyright (C) 2005-2013  Amos Waterland <apw@debian.org>
#
# This file is part of sreplay.
#
# The sreplay program is free software; you can redistribute it
# and/or modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# The sreplay program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with the sreplay program; if not, write to the
# Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.

VERSION = 0
PATCHLEVEL = 2
SUBLEVEL = 9
EXTRAVERSION =
ID=$(VERSION).$(PATCHLEVEL).$(SUBLEVEL)$(EXTRAVERSION)

# Basic build and host configuration.
ARCH = i386
OS = linux

# Build and install directories
O = .
DESTDIR = /usr/local

# Cross-compile support
CROSS_COMPILE =
CC = $(CROSS_COMPILE)gcc
AR = $(CROSS_COMPILE)ar
LD = $(CROSS_COMPILE)ld
STRIP = $(CROSS_COMPILE)strip
OBJDUMP = $(CROSS_COMPILE)objdump
Z = -O0
G = -g

CFLAGS-i386 = -m32
CFLAGS = -Wall -Werror -ffreestanding -nostdinc $(G) $(Z) $(CFLAGS-$(ARCH))

LDFLAGS-i386 = -melf_i386
LDFLAGS = -nostdlib -static $(LDFLAGS-$(ARCH))

aflags-ppc64 = -mregnames
AFLAGS = $(CFLAGS) -Wa,-gdwarf2 $(aflags-$(ARCH))

# Native tools support
MKDIR = mkdir
TAGS = etags
TAR = tar
CP = cp
RM = rm
MV = mv
STRACE = strace

# The objects to be compiled
BINARIES = $(O)/sreplay
DISASS = $(O)/sreplay.s

objs-linux = $(O)/linux/access.o		\
             $(O)/linux/brk.o			\
             $(O)/linux/close.o			\
             $(O)/linux/common.o		\
             $(O)/linux/exit_group.o		\
             $(O)/linux/fcntl.o			\
	     $(O)/linux/fstat64.o		\
             $(O)/linux/futex.o                 \
             $(O)/linux/getdents.o		\
             $(O)/linux/getpid.o		\
             $(O)/linux/getrlimit.o		\
             $(O)/linux/gettimeofday.o          \
             $(O)/linux/ioctl.o			\
             $(O)/linux/lseek.o			\
             $(O)/linux/lstat64.o               \
             $(O)/linux/mmap.o			\
             $(O)/linux/munmap.o		\
             $(O)/linux/open.o			\
             $(O)/linux/read.o			\
             $(O)/linux/readlink.o              \
             $(O)/linux/rt_sigaction.o		\
             $(O)/linux/sendto.o		\
             $(O)/linux/set_tid_address.o	\
             $(O)/linux/stat.o                  \
             $(O)/linux/stat64.o                \
             $(O)/linux/sysctl.o                \
             $(O)/linux/uname.o			\
             $(O)/linux/write.o

objs-linux-i386 = $(O)/linux/set_thread_area.o
objs-linux-ppc64 = $(O)/linux/arch/ppc64/set_thread_area.o

objs-os = objs-$(OS)
objs-os-arch = objs-$(OS)-$(ARCH)

OBJS = $(O)/$(OS)/arch/$(ARCH)/start.o      \
       $(O)/$(OS)/arch/$(ARCH)/syscall.o    \
       $($(objs-os))                        \
       $($(objs-os-arch))                   \
       $(O)/string.o                        \
       $(O)/tokenizer.o                     \
       $(O)/print.o                         \
       $(O)/sreplay.o                       \
       $(O)/main.o

include .config

.config: configure
	./configure

# The phony targets
.PHONY: all dirs clean config tags dist check

# The top-level targets
all: .config dirs $(BINARIES)

# Dump basic build configuration
config:
	@echo Building sreplay for $(ARCH) $(OS) from $(OBJS)

# Build an arch-specific tags database
tags: $(O)/TAGS

CSRC = $(wildcard $(subst .o,.c,$(subst $(O)/,,$(OBJS))))
ASRC = $(wildcard $(subst .o,.S,$(subst $(O)/,,$(OBJS))))
HSRC = $(wildcard *.h $(OS)/arch/$(ARCH)/*.h)
$(O)/TAGS: $(CSRC) $(ASRC) $(HSRC)
	$(call status, ETAGS, $@)
	$(TAGS) $^ -o $@

# Run our regression tests
check: test/copyright.OK test/true.OK

test/copyright.OK: test/copyright.sh clean
	$(call status, TEST, $@)
	test/copyright.sh

test/true.OK: test/true.sh all
	$(call status, TEST, $@)
	test/true.sh

# Make a distribution tarball
dist: clean $(O)/sreplay-$(ID).tar.gz

$(O)/sreplay-$(ID).tar.gz: /tmp/sreplay-$(ID)
	$(call status, TAR, $@)
	cd /tmp && $(TAR) czf sreplay-$(ID).tar.gz sreplay-$(ID)
	$(call status, MV, $@)
	$(MV) /tmp/sreplay-$(ID).tar.gz $@

.PHONY: /tmp/sreplay-$(ID)
/tmp/sreplay-$(ID):
	$(call status, RM, $@)
	$(RM) -rf $@
	$(call status, CP, $@)
	$(CP) -r $(CURDIR) $@
	$(call status, RM, $@/.git $@/.gitignore $@/.config)
	$(RM) -rf $@/.git $@/.gitignore $@/.config

CLEAN = $(BINARIES) $(wildcard  *~ $(OS)/*~ $(OS)/arch/$(ARCH)/*~ test/*~ \
                                *.E $(OS)/*.E $(OS)/arch/$(ARCH)/*.E	  \
				cscope.files $(DISASS) $(OBJS) TAGS	  \
				*.out *.tar.gz *.strace.diff)

clean:
	$(call status, RM, $(CLEAN))
	$(RM) -f $(CLEAN)

MRPROPER = .config $(wildcard *.strace)

mrproper: clean
	$(call status, RM, $(MRPROPER))
	$(RM) -f $(MRPROPER)

$(O)/sreplay: $(OBJS)
	$(call status, LD, $@)
	$(LD) $^ $(LDFLAGS) -o $@

$(O)/%.o: %.c
	$(call status, CC, $@)
	$(CC) $< $(CFLAGS) -I$(OS)/arch/$(ARCH) -I$(OS) -I. -c -o $@

$(O)/%.E: %.c
	$(call status, CC, $@)
	$(CC) $< $(CFLAGS) -I$(OS)/arch/$(ARCH) -I$(OS) -I. -E -o $@


verdefs = -DVERSION='"$(VERSION)"'
verdefs += -DPATCHLEVEL='"$(PATCHLEVEL)"'
verdefs += -DSUBLEVEL='"$(SUBLEVEL)"'
verdefs += -DEXTRAVERSION='"$(EXTRAVERSION)"'
$(O)/main.o: main.c
	$(call status, CC, $@)
	$(CC) $< $(CFLAGS) $(verdefs) -I. -c -o $@

$(O)/%.o: %.S
	$(call status, AS, $@)
	$(CC) $(AFLAGS) -I$(OS)/arch/$(ARCH) -c $< -o $@

$(O)/%.E: %.S
	$(call status, AS, $@)
	$(CC) $(AFLAGS) -I$(OS)/arch/$(ARCH) -E $< -o $@


$(DISASS): %.s: %
	$(call status, OBJDUMP, $@)
	$(OBJDUMP) -d $< > $@

# Rebuild conditions
$(OBJS): $(OS)/arch/$(ARCH)/stub.h
$(OBJS): $(OS)/arch/$(ARCH)/unistd.h
$(OBJS): $(OS)/arch/$(ARCH)/types.h
$(OBJS): print.h sreplay.h string.h tokenizer.h
$(OBJS): .config Makefile

# If V=1, we echo the commands as normal; if V=2, show reason for rebuild.
V = 0
ifneq "$(V)" "1"
.SILENT:
status = printf " %-7s %s\n" $(1) "$(subst $(O)/,,$(2))"
ifeq "$(V)" "2"
status = printf " %-7s %s [%s]\n" \
                $(1) "$(subst $(O)/,,$(2))"  $(notdir $(firstword $?))
endif
endif

# Now that OBJS is totally expanded, generate our directory list from it.
DIRS = $(sort $(foreach obj, $(OBJS), $(dir $(obj))))
dirs: $(DIRS)

# This is always the last rule: it needs OBJS to be totally expanded.
$(DIRS):
	$(call status, MKDIR, $@)
	$(MKDIR) -p $@
