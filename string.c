/* Copyright (C) 2005-2013  Amos Waterland <apw@debian.org>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <print.h>

int long2str(const long *in, char *out, const int len)
{
    char c, buff[32];
    long val = *in, sign = 1;
    int i, j;

    if (val == 0) {
        out[0] = '0';
        out[1] = 0;
        return 0;
    }

    if (val < 0) {
        sign = -1;
        val *= -1;
    }

    for (i = 0; val; i++) {
        if (i >= sizeof(buff))
            return -1;
        c = val % 10;
        c += '0';
        buff[i] = c;
        val /= 10;
    }

    if (sign == -1) {
        buff[i++] = '-';
    }

    for (j = 0; j < len && i > 0; j++, i--) {
        out[j] = buff[i - 1];
    }

    out[j] = 0;

    return 0;
}

int str2uint(const char *in, unsigned int *out, int base)
{
    int r;
    long val;

    r = str2long(in, &val, base);
    if (r)
        return -1;

    *out = val;

    return 0;
}

int str2int(const char *in, int *out, int base)
{
    int r;
    long val;

    r = str2long(in, &val, base);
    if (r)
        return -1;

    *out = val;

    return 0;
}

int str2long(const char *in, long *out, int base)
{
    int i, j, l, n;
    long r = 0;
    char c;

    if (base != 10 && base != 16)
        return -1;

    l = strlen(in) - 1;

    for (i = l, j = 0; i >= 0; i--, j++) {
        switch (c = in[i]) {
        case '0':
            n = 0;
            break;
        case '1':
            n = 1;
            break;
        case '2':
            n = 2;
            break;
        case '3':
            n = 3;
            break;
        case '4':
            n = 4;
            break;
        case '5':
            n = 5;
            break;
        case '6':
            n = 6;
            break;
        case '7':
            n = 7;
            break;
        case '8':
            n = 8;
            break;
        case '9':
            n = 9;
            break;
        case 'a':
        case 'A':
            n = 10;
            break;
        case 'b':
        case 'B':
            n = 11;
            break;
        case 'c':
        case 'C':
            n = 12;
            break;
        case 'd':
        case 'D':
            n = 13;
            break;
        case 'e':
        case 'E':
            n = 14;
            break;
        case 'f':
        case 'F':
            n = 15;
            break;
        case '-':
            n = -1;
            break;
        case 'x':
            if (base != 16)
                return -1;
            continue;
            break;
        default:
            prints("sreplay: string parse error in: `%s'\n", in);
            return -1;
        }

        if (n >= 0) {
            r = (n * pow(base, j)) + r;
        } else {
            r = n * r;
        }
    }

    *out = r;
    return 0;
}

int str2ptr(const char *str, void **ptr)
{
    long val;
    int r;

    switch (str[0]) {
    case 'N':
        if (strcmp(str, "NULL") == 0) {
            *ptr = 0x0;
            return 0;
        }
        break;
    case '0':
        r = str2long(str, &val, 16);
        if (r)
            return -1;
        *ptr = (void *) val;
        return 0;
        break;
    }

    return -1;
}

long pow(long x, long y)
{
    long i, r = x;

    if (y == 0)
        return 1;

    for (i = 1; i < y; i++)
        r *= x;

    return r;
}

int strcmp(const char *s1, const char *s2)
{
    int i;

    for (i = 0; s1[i] && s2[i] && (s1[i] == s2[i]); i++);

    if (s1[i] == '\0' && s2[i] == '\0')
        return 0;

    return 1;
}

int strncmp(const char *s1, const char *s2, int n)
{
    int i;

    for (i = 0; i < n && s1[i] && s2[i] && (s1[i] == s2[i]); i++);

    if (i == n)
        return 0;

    return 1;
}

int strlen(const char *s)
{
    int i;

    for (i = 0; s[i]; i++);

    return i;
}

long atol(char const *str)
{
    long val;

    str2long(str, &val, 10);

    return val;
}

void *memset(void *s, int c, int n)
{
    int i;
    char *p;

    for (i = 0, p = s; i < n; i++) {
        p[i] = 0;
    }

    return s;
}

/* Most of the compiler support routines used by GCC are present in
   `libgcc', but there are a few exceptions.  GCC requires the
   freestanding environment provide `memcpy', `memmove', `memset' and
   `memcmp'. */
void *memcpy(void *dest, const void *src, int n)
{
    int i;
    char *d, *s;

    for (i = 0, d = dest, s = (char *) src; i < n; i++) {
        d[i] = s[i];
    }

    return dest;
}
