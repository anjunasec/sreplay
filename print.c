/* Copyright (C) 2005-2013  Amos Waterland <apw@debian.org>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <print.h>
#include <string.h>

int write(int fd, const void *buf, int count);

static char buffer[2048];

int out(const char *str)
{
    int i, r;
    int next = strlen(buffer);
    int remainder = sizeof(buffer) - next;
    int position;

    for (i = 0; str[i] && i < remainder; i++) {
        int j = next + i;
        buffer[j] = str[i];
    }

    if (i >= remainder) {
        while (1);
    }

    position = strlen(buffer) - 1;

    if (buffer[position] == '\n') {
        r = write(1, buffer, position + 1);
        if (r < 0) {
            while (1);
        }
        memset(buffer, 0, sizeof(buffer));
    }

    return 0;
}

int print(const char *format)
{
    int r;
    r = out(format);
    return r;
}

int prints(const char *format, const void *arg)
{
    int i, j, k, r;
    char buff[2048];

    for (i = 0, j = 0; i < sizeof(buff) && format[j]; j++) {
        if (format[j] == '%') {
            ++j;
            if (format[j] == 's') {
                const char *str = arg;
                for (k = 0; str[k]; k++)
                    buff[i++] = str[k];
            } else if (format[j] == 'd') {
                const long *val = arg;
                char tmp[32];
                k = long2str(val, tmp, sizeof(tmp));
                if (k)
                    return -1;
                for (k = 0; tmp[k]; k++)
                    buff[i++] = tmp[k];
            }
        } else {
            buff[i++] = format[j];
        }
    }

    buff[i] = '\0';
    r = out(buff);

    return r;
}
