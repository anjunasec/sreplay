`sreplay' NEWS -- history of user-visible changes.
Copyright (C) 2005-2013  Amos Waterland.
See the end for copying conditions.

Please send `sreplay' bug reports, questions, and suggestions to
<apw@debian.org>.

2007-06-20  Version 0.2.9

 * Support for lstat64, readlink, gettimeofday and stat system calls.

2007-06-05  Version 0.2.8

 * Support for stat64 system call, additional signals supported by
   rt_sigaction handler.

2007-05-29  Version 0.2.7

 * Support for the sendto system call.  Parse octal escapes in input.
   Deal with strace umovestr errors in input.

2007-05-15  Version 0.2.6

 * Error handling in the case of invalid input was improved.  Signal
   delivery is now dealt with in input traces.

2007-04-05  Version 0.2.5

 * Many new system calls supported.  Program can replay /bin/ls on
   most supported systems.

2007-02-16  Version 0.2.4

 * Fix parenthesis error in parsing logic.  Program can replay trace
   of the standard echo utility.

2007-02-14  Version 0.2.3

 * First public release.  Program is capable of replaying simple
   dynamically-linked programs on i386 and ppc64 Linux.

----------------------------------------------------------------------
Copyright information:

Copyright (C) 2005-2013  Amos Waterland

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.
