/* Copyright (C) 2005-2013  Amos Waterland <apw@debian.org>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#ifndef REPLAY_H
#define REPLAY_H

enum { SYSCALL, ARGUMENT, EQUALS, RETURN, DECODE, EOF, ERROR };

#define MAXSYSCALLARGS 6

struct syscall {
    char name[32];
    int nargs;
    char args[MAXSYSCALLARGS][1024];
    char retval[32];
    char errno[64];
    long exitcode;
    int success;
    const char *context;
};

#define EXECED 0x1

struct state {
    unsigned int flags;
};

int parse_fd(const char *str, unsigned int *fd);

int replay(int argc, char **argv, int dry);

long sys_access(struct syscall *call);
long sys_open(struct syscall *call);
long sys_fstat64(struct syscall *call);
long sys_fstat(struct syscall *call);
long sys_close(struct syscall *call);
long sys_mmap(struct syscall *call);
long sys_read(struct syscall *call);
long sys_set_thread_area(struct syscall *call);
long sys_set_tid_address(struct syscall *call);
long sys_munmap(struct syscall *call);
long sys_mmap2(struct syscall *call);
long sys_mprotect(struct syscall *call);
long sys_uname(struct syscall *call);
long sys_brk(struct syscall *call);
long sys_write(struct syscall *call);
long sys_exit_group(struct syscall *call);
long sys_lseek(struct syscall *call);
long sys_rt_sigaction(struct syscall *call);
long sys_rt_sigprocmask(struct syscall *call);
long sys_getrlimit(struct syscall *call);
long sys_ioctl(struct syscall *call);
long sys_fcntl64(struct syscall *call);
long sys_getdents64(struct syscall *call);
long sys_getpid(struct syscall *call);
long sys_sysctl(struct syscall *call);
long sys_fcntl(struct syscall *call);
long sys_futex(struct syscall *call);
long sys_stat64(struct syscall *call);
long sys_sendto(struct syscall *call);
long sys_lstat64(struct syscall *call);
long sys_readlink(struct syscall *call);
long sys_gettimeofday(struct syscall *call);
long sys_stat(struct syscall *call);

enum { ARGERR = -1, SYSERR = -2 };

#endif
