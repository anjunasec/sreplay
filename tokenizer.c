/* Copyright (C) 2005-2013  Amos Waterland <apw@debian.org>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include "tokenizer.h"
#include "sreplay.h"
#include "string.h"
#include "print.h"

int init_token(struct tokens *tokens, int argc, char **argv)
{
    tokens->argc = argc;
    tokens->argv = argv;
    tokens->x = 0;
    tokens->y = 0;
    tokens->quote = 0;
    tokens->brace = 0;
    tokens->bracket = 0;
    tokens->state = SYSCALL;
    return 0;
}

static int next(struct tokens *tokens, unsigned int flags)
{
    int c = -1;

    while (tokens->x < tokens->argc) {
        c = tokens->argv[tokens->x][tokens->y];

        if (c) {
            tokens->y++;
            break;
        } else {
            c = -1;
            tokens->x++;
            tokens->y = 0;
            if (flags & TOKENSTOP)
                return c = 0;
            /* We behave like echo and just insert a space.  */
            if (tokens->state == ARGUMENT &&
                (tokens->quote || tokens->bracket || tokens->brace)) {
                return c = ' ';
            }
        }
    }

    return c;
}

static int back(struct tokens *tokens)
{
    if (tokens->y) {
        tokens->y--;
    } else {
        tokens->x--;
        tokens->y = strlen(tokens->argv[tokens->x]);
    }

    return 0;
}

int next_token(struct tokens *tokens, char *token, int len)
{
    int c, i;

    switch (tokens->state) {
    case SYSCALL:
        for (i = 0;;) {
            c = next(tokens, 0);

            if (c < 0) {
                return tokens->state = EOF;
            }
            if (c == 0) {
                prints("sreplay: expected `(', got junk near `%s'\n",
                       token);
                return tokens->state = ERROR;
            }
            if (c == '(') {
                token[i] = '\0';
                return tokens->state = ARGUMENT;
            }
            /* Consume entire line denoting signal delivery.  */
            if (c == '-') {
                int count = 6 - 1;
                do {
                    c = next(tokens, c);
                    if (c == '-')
                        count--;
                } while (c >= 0 && count);
                continue;
            }
            if (i >= len - 1) {
                prints("sreplay: not enough space for `%s'\n", token);
                return tokens->state = ERROR;
            }

            token[i++] = c;
        }
        break;
    case ARGUMENT:
        for (i = 0;;) {
            c = next(tokens, 0);

            if (c < 0) {
                prints("sreplay: got EOF, not `)' near `%s'\n", token);
                return tokens->state = ERROR;
            } else if (c == '"') {
                tokens->quote = (tokens->quote + 1) % 2;
                continue;
            } else if (c == '\\') {
                c = next(tokens, 0);
                if (c == 'n')
                    c = '\n';
                else if (c == 't')
                    c = '\t';
                else if (c == 'r')
                    c = '\r';
                else if (c == '0')
                    c = 0;
                else if ('0' <= c && c <= '9') {
                    int x, y, z, q;
                    /* Grab the next three characters.  */
                    x = c;
                    y = next(tokens, 0);
                    z = next(tokens, 0);
                    /* Ambiguity here regarding three-character escape.  */
                    if (('0' <= y && y <= '9') && ('0' <= z && z <= '9')) {
                        /* Convert to number from ASCII.  */
                        x = x - '0';
                        y = y - '0';
                        z = z - '0';
                        /* Shift their binary representation.  */
                        x = x << 6;
                        y = y << 3;
                        z = z << 0;
                        /* Form the final character from a bitwise OR.  */
                        q = x | y | z;
                        /* Octal 177 is decimal 127 is hex 7F.  */
                        c = q;
                    } else {
                        back(tokens);
                        back(tokens);
                        x = x - '0';
                        q = x;
                        c = q;
                    }
                }
            } else if (c == ',' && !tokens->quote &&
                       !tokens->brace && !tokens->bracket) {
                token[i] = '\0';
                /* Special case.  */
                if (strncmp(token, "umovestr:Input/outputerror", 26) == 0) {
                    int j;
                    for (j = 26; token[j]; j++) {
                        token[j - 26] = token[j];
                    }
                    token[j - 26] = 0;
                }
                return tokens->state = ARGUMENT;
            } else if (c == '{' && !tokens->quote) {
                tokens->brace++;
            } else if (c == '}' && !tokens->quote) {
                if (--tokens->brace < 0)
                    return tokens->state = ERROR;
            } else if (c == '[' && !tokens->quote) {
                tokens->bracket++;
            } else if (c == ']' && !tokens->quote) {
                if (tokens->bracket > 0) {
                    tokens->bracket--;
                } else {
                    return tokens->state = ERROR;
                }
            } else if (c == '(' && !tokens->quote) {
                tokens->paren++;
            } else if (c == ')' && !tokens->quote) {
                if (tokens->paren > 0) {
                    tokens->paren--;
                } else {
                    token[i] = '\0';
                    return tokens->state = EQUALS;
                }
            }

            if (i >= len - 1) {
                prints("sreplay: not enough space for `%s'\n", token);
                return tokens->state = ERROR;
            }
            token[i++] = c;
        }
        break;
    case EQUALS:
        for (i = 0;;) {
            c = next(tokens, 0);

            if (c == '=') {
                return tokens->state = RETURN;
            } else if (c == ' ') {
                continue;
            } else if (c < 0) {
                print("sreplay: expected `=', got EOF\n");
                return tokens->state = ERROR;
            } else {
                char err[2] = { c, '\0' };
                prints("sreplay: expected `=', got junk near `%s'\n", err);
                return tokens->state = ERROR;
            }
        }
        break;
    case RETURN:
        for (i = 0;;) {
            c = next(tokens, TOKENSTOP);

            if (c < 0 && i == 0) {
                print("sreplay: expected return code, got EOF\n");
                return tokens->state = ERROR;
            }
            if (c == 0 || c == ';' || c == ' ') {
                if (i > 0) {
                    token[i] = '\0';

                    while ((c = next(tokens, 0)) == ' ');

                    if (c == 'E') {
                        back(tokens);
                        return tokens->state = DECODE;
                    } else if (c == '(') {
                        do {
                            c = next(tokens, TOKENSTOP);
                        } while (c > 0 && c != ')');
                    } else {
                        back(tokens);
                        return tokens->state = SYSCALL;
                    }
                } else {
                    continue;
                }
            }
            if (i >= len - 1) {
                prints("sreplay: not enough space for `%s'\n", token);
                return tokens->state = ERROR;
            }

            token[i++] = c;
        }
        break;
    case DECODE:
        for (i = 0;;) {
            c = next(tokens, 0);

            if (c < 0) {
                print("sreplay: expected `)', got EOF\n");
                return tokens->state = ERROR;
            }
            if (c == ')') {
                return tokens->state = SYSCALL;
            }

            if (i >= len - 1) {
                prints("sreplay: not enough space for `%s'\n", token);
                return tokens->state = ERROR;
            }

            token[i++] = c;

            if (token[i - 1] == '(')
                token[i - 1] = 0;
        }
        break;
    }

    return tokens->state = ERROR;
}
