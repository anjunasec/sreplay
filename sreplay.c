/* Copyright (C) 2005-2013  Amos Waterland <apw@debian.org>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <sreplay.h>
#include <string.h>
#include <tokenizer.h>
#include <print.h>

long parse_errno(struct syscall *call)
{
    long r, o;
    const char *retval = call->retval;
    const char *errno = call->errno;

    if (retval[0] == '?') {
        /* Probably the return code from exit, so let it pass.  */
        r = 0;
    } else if (retval[1] == 'x') {
        /* A pointer was returned.  */
        r = str2ptr(retval, (void *) &o);
    } else {
        /* A regular number was returned.  */
        r = str2long(retval, &o, 10);
    }

    if (r)
        return r;

    if (o == -1) {
        if (strcmp("ENOENT", errno) == 0) {
            call->exitcode = -2;
        } else if (strcmp("ENOMEM", errno) == 0) {
            call->exitcode = -12;
        } else if (strcmp("ENOSYS", errno) == 0) {
            call->exitcode = -38;
        } else if (strcmp("ENOTTY", errno) == 0) {
            call->exitcode = -25;
        } else if (strcmp("EINVAL", errno) == 0) {
            call->exitcode = -22;
        } else {
            return -1;
        }
    } else {
        call->exitcode = o;
    }

    return 0;
}

static void dump(struct syscall *const current)
{
    int i;

    print("--- begin parse dump ---\n");
    prints(" syscall  = %s\n", current->name);
    for (i = 0; i < current->nargs; i++) {
        prints(" argument = %s\n", current->args[i]);
    }
    prints(" retval   = %s\n", current->retval);
    if (current->errno[0]) {
        prints(" errno    = %s\n", current->errno);
    }
    print("--- end parse dump ---\n");
}

static int syscall(struct syscall *call, struct state *state)
{
    const char *name = call->name;
    unsigned int *flags = &state->flags;
    long r;

    r = parse_errno(call);

    if (r) {
        dump(call);
        prints("sreplay: error parsing return code: %s\n", call->errno);
        return -1;
    }

    switch (name[0]) {
    case 'a':
        if (strcmp(name, "access") != 0)
            goto unsupported;
        r = sys_access(call);
        break;
    case 'b':
        if (strcmp(name, "brk") != 0)
            goto unsupported;
        r = sys_brk(call);
        break;
    case 'c':
        if (strcmp(name, "close") != 0)
            goto unsupported;
        r = sys_close(call);
        break;
    case 'e':
        if (strcmp(name, "exit_group") == 0) {
            call->exitcode = 0;
            r = sys_exit_group(call);
        } else if (strcmp(name, "execve") == 0 && !(*flags & EXECED)) {
            *flags &= EXECED;
            call->success = 1;
        } else {
            goto unsupported;
        }
        break;
    case 'g':
        if (strcmp(name, "getrlimit") == 0) {
            r = sys_getrlimit(call);
        } else if (strcmp(name, "getdents64") == 0) {
            r = sys_getdents64(call);
        } else if (strcmp(name, "getpid") == 0) {
            r = sys_getpid(call);
        } else if (strcmp(name, "gettimeofday") == 0) {
            r = sys_gettimeofday(call);
        } else {
            goto unsupported;
        }
        break;
    case 'f':
        if (strcmp(name, "fstat64") == 0) {
            r = sys_fstat64(call);
        } else if (strcmp(name, "fstat") == 0) {
            r = sys_fstat(call);
        } else if (strcmp(name, "fcntl64") == 0) {
            r = sys_fcntl64(call);
        } else if (strcmp(name, "fcntl") == 0) {
            r = sys_fcntl(call);
        } else if (strcmp(name, "futex") == 0) {
            r = sys_futex(call);
        } else {
            goto unsupported;
        }
        break;
    case 'i':
        if (strcmp(name, "ioctl") == 0) {
            r = sys_ioctl(call);
        } else {
            goto unsupported;
        }
        break;
    case 'l':
        if (strcmp(name, "lseek") == 0) {
            r = sys_lseek(call);
        } else if (strcmp(name, "lstat64") == 0) {
            r = sys_lstat64(call);
        } else {
            goto unsupported;
        }
        break;
    case 'm':
        if (strcmp(name, "munmap") == 0) {
            r = sys_munmap(call);
        } else if (strcmp(name, "mmap2") == 0) {
            r = sys_mmap2(call);
        } else if (strcmp(name, "mmap") == 0) {
            r = sys_mmap(call);
        } else if (strcmp(name, "mprotect") == 0) {
            r = sys_mprotect(call);
        } else {
            goto unsupported;
        }
        break;
    case 'o':
        if (strcmp(name, "old_mmap") == 0) {
            r = sys_mmap(call);
        } else if (strcmp(name, "open") == 0) {
            r = sys_open(call);
        } else {
            goto unsupported;
        }
        break;
    case 'r':
        if (strcmp(name, "read") == 0) {
            r = sys_read(call);
        } else if (strcmp(name, "rt_sigaction") == 0) {
            r = sys_rt_sigaction(call);
        } else if (strcmp(name, "rt_sigprocmask") == 0) {
            r = sys_rt_sigprocmask(call);
        } else if (strcmp(name, "readlink") == 0) {
            r = sys_readlink(call);
        } else {
            goto unsupported;
        }
        break;
    case 's':
        if (strcmp(name, "set_thread_area") == 0) {
            r = sys_set_thread_area(call);
        } else if (strcmp(name, "set_tid_address") == 0) {
            r = sys_set_tid_address(call);
        } else if (strcmp(name, "stat64") == 0) {
            r = sys_stat64(call);
        } else if (strcmp(name, "sendto") == 0) {
            r = sys_sendto(call);
        } else if (strcmp(name, "stat") == 0) {
            r = sys_stat(call);
        } else {
            goto unsupported;
        }
        break;
    case 'u':
        if (strcmp(name, "uname") != 0)
            goto unsupported;
        r = sys_uname(call);
        break;
    case 'w':
        if (strcmp(name, "write") != 0)
            goto unsupported;
        r = sys_write(call);
        break;
    case '_':
        if (strcmp(name, "_sysctl") == 0) {
            r = sys_sysctl(call);
        } else {
            goto unsupported;
        }
        break;
    default:
        goto unsupported;
    }

    /* Warn if our system call implementation did not return to us
       the same code that the one in the trace did.  Note that an
       implementation sys_foo may fix up the actual return code from
       the real system call foo if it determines good cause.  */
    if (r != call->exitcode) {
        prints("sreplay: warning: %s returned ", name);
        prints("%d ", &r);
        prints("instead of %d\n", &call->exitcode);
    }

    /* Bail out if error parsing or executing the syscall.  */
    if (call->success != 1) {
        dump(call);
        prints("sreplay: error: parse failure for: %s", call->name);
        if (call->context) {
            prints(" near: %s", call->context);
        }
        print("\n");
        return -1;
    }

    return 0;

  unsupported:
    prints("sreplay: error: unsupported syscall: %s\n", name);
    return -1;
}

int replay(int argc, char **argv, int dry)
{
    int i, r, q;
    struct tokens tokens;
    struct syscall current;
    struct state state = {.flags = 0x0 };

    init_token(&tokens, argc, argv);

    do {
        /* First we are expecting a system call.  */
        memset(&current, 0, sizeof(current));
        r = next_token(&tokens, current.name, sizeof(current.name));
        if (r == ERROR) {
            print("sreplay: parse error: missing syscall token\n");
            return -1;
        }
        if (r == EOF) {
            if (strncmp(current.name, "Process ", 8)) {
                /* Ignore the message about process detached.  */
                return 0;
            } else if (current.name[0]) {
                prints("sreplay: trailing garbage near `%s'\n",
                       current.name);
                return -1;
            } else {
                return 0;
            }
        }

        /* Catch parse errors early.  */
        if (current.name[0] == 0) {
            prints("sreplay: parse error near: %s\n",
                   &tokens.argv[tokens.x][0]);
            return -1;
        }

        /* Now we are expecting one or more arguments.  */
        for (i = 0, current.nargs = 0; i < MAXSYSCALLARGS; i++) {
            int lim = sizeof(current.args[i]);
            r = next_token(&tokens, current.args[i], lim);
            if (r == ERROR) {
                print("sreplay: parse error: missing argument token\n");
                return -1;
            }
            current.nargs++;
            if (r == EQUALS)
                break;
        }

        /* Probably a parse error.  */
        if (i >= MAXSYSCALLARGS) {
            dump(&current);
            print("sreplay: too many arguments to system call\n");
            return -1;
        }

        /* Then we are expecting an equal sign.  */
        r = next_token(&tokens, 0, 0);
        if (r == ERROR) {
            dump(&current);
            print("sreplay: parse error: missing equal sign token\n");
            return -1;
        }

        /* Last we are expecting a return code.  */
        r = next_token(&tokens, current.retval, sizeof(current.retval));
        if (r != SYSCALL && r != DECODE && r != EOF) {
            print("sreplay: parse error: missing return code token\n");
            return -1;
        }

        /* We might have to expect some retval errno decoding.  */
        if (r == DECODE) {
            r = next_token(&tokens, current.errno, sizeof(current.errno));
            if (r != SYSCALL) {
                print("sreplay: parse error: missing decode token\n");
                return -1;
            }
        }

        /* If doing a dry run, just print the system call.  */
        if (dry) {
            prints("SYS: %s\n", current.name);
            for (i = 0; i < current.nargs; i++) {
                prints("ARG: %s\n", current.args[i]);
            }
            prints("RET: %s\n\n", current.retval);
            continue;
        }

        /* Fire off the system call.  */
        q = syscall(&current, &state);

        /* Bail out if an error occurred.  */
        if (q) {
            return -1;
        }
    } while (r != EOF);

    return 0;
}
