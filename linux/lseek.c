/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>

typedef long int off_t;

#define SEEK_SET        0       /* Seek from beginning of file.  */
#define SEEK_CUR        1       /* Seek from current position.  */
#define SEEK_END        2       /* Seek from end of file.  */

static int lseek_whence(const char *str, int *whence)
{
    const char *s = str;
    *whence = 0x0;

    while (1) {
        if (strncmp("SEEK_SET", s, 8) == 0) {
            *whence |= SEEK_SET;
            s = s + 8;
        } else if (strncmp("SEEK_CUR", s, 8) == 0) {
            *whence |= SEEK_CUR;
            s = s + 8;
        } else if (strncmp("SEEK_END", s, 8) == 0) {
            *whence |= SEEK_END;
            s = s + 8;
        } else {
            return -1;
        }
        if (s[0] == '|')
            s++;
        if (s[0] == 0)
            break;
    }

    return 0;
}

off_t lseek(int fildes, off_t offset, int whence);

long sys_lseek(struct syscall *call)
{
    long fd;
    off_t offset;
    int r, whence;

    r = str2long(call->args[0], &fd, 10);
    if (r)
        return -1;

    r = str2long(call->args[1], &offset, 10);
    if (r)
        return -1;

    r = lseek_whence(call->args[2], &whence);
    if (r)
        return -1;

    r = lseek(fd, offset, whence);

    call->success = 1;
    return r;
}
