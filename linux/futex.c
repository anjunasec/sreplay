/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <types.h>

long futex(u32 * uaddr, int op, u32 val,
           struct timespec *utime, u32 * uaddr2, u32 val3);

static unsigned int uaddr;

long sys_futex(struct syscall *call)
{
    long r;
    int op = 0;
    unsigned int val = 0;
    long tmp;

    /* First argument.  */
    uaddr = 0;

    /* Second argument.  */
    if (strncmp("FUTEX_WAKE", call->args[1], 10) == 0) {
        op = FUTEX_WAKE;
    } else {
        call->context = call->args[0];
        return -1;
    }

    /* Third argument.  */
    r = str2long(call->args[2], &tmp, 10);
    if (r) {
        call->context = call->args[0];
        return -1;
    }
    val = tmp;

    /* System call.  */
    r = futex(&uaddr, op, val, 0, 0, 0);

    call->success = 1;
    return 0;
}
