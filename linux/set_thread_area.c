/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>

struct user_desc {
    unsigned int entry_number;
    unsigned long int base_addr;
    unsigned int limit;
    unsigned int seg_32bit:1;
    unsigned int contents:2;
    unsigned int read_exec_only:1;
    unsigned int limit_in_pages:1;
    unsigned int seg_not_present:1;
    unsigned int useable:1;
    unsigned int empty:25;
};

static int parse_desc(const char *str, struct user_desc *u_info)
{
    const char *s = str;
    char buff[32];
    int i, r;
    long val;
    void *ptr;

    memset(buff, 0, sizeof(buff));

    while (1) {
        if (strncmp("{entry_number:", s, 14) == 0) {
            s = s + 14;
            for (i = 0; *s && *s != ',' && i < sizeof(buff); i++) {
                if (*s == ' ') {
                    s++;
                    continue;
                }
                if (*s == '-' && *(s + 1) == '>') {
                    s += 2;
                    while (*s && *s != ',')
                        s++;
                    continue;
                }
                buff[i] = *s++;
            }
            r = str2long(buff, &val, 10);
            if (r)
                return -1;
            u_info->entry_number = val;
        } else if (strncmp("base_addr:", s, 10) == 0) {
            s = s + 10;
            for (i = 0; i < sizeof(buff) && *s && *s != ','; i++) {
                buff[i] = *s++;
            }
            buff[i] = 0;
            r = str2ptr(buff, &ptr);
            if (r)
                return -1;
            u_info->base_addr = (unsigned long) ptr;
        } else if (strncmp("limit:", s, 6) == 0) {
            s = s + 6;
            for (i = 0; i < sizeof(buff) && *s && *s != ','; i++) {
                buff[i] = *s++;
            }
            buff[i] = 0;
            r = str2long(buff, &val, 10);
            if (r)
                return -1;
            u_info->limit = val;
        } else if (strncmp("seg_32bit:", s, 10) == 0) {
            s = s + 10;
            for (i = 0; i < sizeof(buff) && *s && *s != ','; i++) {
                buff[i] = *s++;
            }
            buff[i] = 0;
            r = str2long(buff, &val, 10);
            if (r)
                return -1;
            u_info->seg_32bit = val;
        } else if (strncmp("contents:", s, 9) == 0) {
            s = s + 9;
            for (i = 0; i < sizeof(buff) && *s && *s != ','; i++) {
                buff[i] = *s++;
            }
            buff[i] = 0;
            r = str2long(buff, &val, 10);
            if (r)
                return -1;
            u_info->contents = val;
        } else if (strncmp("read_exec_only:", s, 15) == 0) {
            s = s + 15;
            for (i = 0; i < sizeof(buff) && *s && *s != ','; i++) {
                buff[i] = *s++;
            }
            buff[i] = 0;
            r = str2long(buff, &val, 10);
            if (r)
                return -1;
            u_info->read_exec_only = val;
        } else if (strncmp("limit_in_pages:", s, 15) == 0) {
            s = s + 15;
            for (i = 0; i < sizeof(buff) && *s && *s != ','; i++) {
                buff[i] = *s++;
            }
            buff[i] = 0;
            r = str2long(buff, &val, 10);
            if (r)
                return -1;
            u_info->limit_in_pages = val;
        } else if (strncmp("seg_not_present:", s, 16) == 0) {
            s = s + 16;
            for (i = 0; i < sizeof(buff) && *s && *s != ','; i++) {
                buff[i] = *s++;
            }
            buff[i] = 0;
            r = str2long(buff, &val, 10);
            if (r)
                return -1;
            u_info->seg_not_present = val;
        } else if (strncmp("useable:", s, 8) == 0) {
            s = s + 8;
            for (i = 0; i < sizeof(buff) && *s && *s != '}'; i++) {
                buff[i] = *s++;
            }
            buff[i] = 0;
            r = str2long(buff, &val, 10);
            if (r)
                return -1;
            u_info->useable = val;
        } else if (s[0] == ' ') {
            s++;
        } else {
            return -1;
        }
        if (s[0] == ',')
            s++;
        if (s[0] == '}')
            s++;
        if (s[0] == 0)
            break;
    }

    return 0;
}

int set_thread_area(struct user_desc *u_info);

long sys_set_thread_area(struct syscall *call)
{
    int r;
    struct user_desc u_info;

    r = parse_desc(call->args[0], &u_info);
    if (r)
        return -1;

    r = set_thread_area(&u_info);

    call->success = 1;
    return r;
}
