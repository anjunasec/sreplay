/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>

#define RLIMIT_CPU          0   /* CPU time in ms */
#define RLIMIT_FSIZE        1   /* Maximum filesize */
#define RLIMIT_DATA         2   /* max data size */
#define RLIMIT_STACK        3   /* max stack size */
#define RLIMIT_CORE         4   /* max core file size */
#define RLIMIT_RSS          5   /* max resident set size */
#define RLIMIT_NPROC        6   /* max number of processes */
#define RLIMIT_NOFILE       7   /* max number of open files */
#define RLIMIT_MEMLOCK      8   /* max locked-in-memory address space */
#define RLIMIT_AS           9   /* address space limit */
#define RLIMIT_LOCKS       10   /* maximum file locks held */
#define RLIMIT_SIGPENDING  11   /* max number of pending signals */
#define RLIMIT_MSGQUEUE    12   /* maximum bytes in POSIX mqueues */
#define RLIMIT_NICE        13   /* max nice prio allowed to raise to */
#define RLIMIT_RTPRIO      14   /* maximum realtime priority */
#define RLIM_NLIMITS       15
#define RLIM_INFINITY	   (~0UL)

static int parse_resource(const char *str, unsigned int *resource)
{
    const char *s = str;

    while (1) {
        if (strncmp("RLIMIT_CPU", s, 10) == 0) {
            *resource = RLIMIT_CPU;
            s = s + 10;
        } else if (strncmp("RLIMIT_STACK", s, 12) == 0) {
            *resource = RLIMIT_STACK;
            s = s + 12;
        } else if (s[0] == ' ') {
            s++;
        }
        if (s[0] == 0) {
            break;
        } else {
            return -1;
        }
    }

    return 0;
}

struct rlimit {
    unsigned long rlim_cur;
    unsigned long rlim_max;
};

long getrlimit(unsigned int resource, struct rlimit *rlim);

long sys_getrlimit(struct syscall *call)
{
    long r;
    unsigned int resource = 0;
    struct rlimit rlim;

    /* First argument.  */
    r = parse_resource(call->args[0], &resource);
    if (r)
        return -1;

    /* Second argument.  */
    memset(&rlim, 0, sizeof(rlim));

    /* Fire off the system call.  */
    r = getrlimit(resource, &rlim);

    call->success = 1;
    return r;
}
