/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <sreplay.h>
#include <types.h>
#include <string.h>

enum {
    CTL_KERN = 1,               /* General kernel info and control */
    CTL_VM = 2,                 /* VM management */
    CTL_NET = 3,                /* Networking */
    /* was CTL_PROC */
    CTL_FS = 5,                 /* Filesystems */
    CTL_DEBUG = 6,              /* Debugging */
    CTL_DEV = 7,                /* Devices */
    CTL_BUS = 8,                /* Busses */
    CTL_ABI = 9,                /* Binary emulation */
    CTL_CPU = 10                /* CPU stuff (speed scaling, etc) */
};


enum {
    KERN_OSTYPE = 1,            /* string: system version */
    KERN_OSRELEASE = 2,         /* string: system release */
    KERN_OSREV = 3,             /* int: system revision */
    KERN_VERSION = 4,           /* string: compile time info */
    KERN_SECUREMASK = 5,        /* struct: maximum rights mask */
    KERN_PROF = 6,              /* table: profiling information */
    KERN_NODENAME = 7,
    KERN_DOMAINNAME = 8
};

struct __sysctl_args {
    int *name;
    int nlen;
    void *oldval;
    size_t *oldlenp;
    void *newval;
    size_t newlen;
    unsigned long __unused[4];
};

long _sysctl(struct __sysctl_args *args);

long sys_sysctl(struct syscall *call)
{
    long r;
    struct __sysctl_args args;
    int name[32];
    char buff[256];
    int nlen = 0;
    size_t len = sizeof(buff);
    char *s;

    memset(&args, 0, sizeof(args));
    memset(&name, 0, sizeof(name));

    s = call->args[0];

    while (1) {
        if (strncmp("CTL_KERN", s, 8) == 0) {
            name[0] = CTL_KERN;
            s = s + 8;
        } else if (strncmp("KERN_VERSION", s, 12) == 0) {
            name[1] = KERN_VERSION;
            s = s + 12;
        } else if (s[0] == '2') {
            nlen = 2;
            s++;
        } else if (s[0] == ' ') {
            s++;
        } else if (s[0] == ',') {
            s++;
            if (nlen)
                break;
        } else if (s[0] == '{') {
            s++;
        } else if (s[0] == '}') {
            s++;
        } else if (s[0] == 0) {
            break;
        } else {
            s++;
        }
    }

    args.name = name;
    args.nlen = nlen;
    args.oldval = buff;
    args.oldlenp = &len;
    args.newval = 0;
    args.newlen = 0;

    /* System call.  */
    r = _sysctl(&args);

    call->success = 1;
    return r;
}
