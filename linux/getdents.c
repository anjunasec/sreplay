/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <types.h>

struct linux_dirent64 {
    u64 d_ino;
    s64 d_off;
    unsigned short d_reclen;
    unsigned char d_type;
    char d_name[0];
};

static int parse_dirent(const char *str, struct linux_dirent64 *dirent)
{
    memset(dirent, 0, sizeof(*dirent));

    return 0;
}

static int parse_count(const char *str, unsigned int *count)
{
    long r, val;

    r = str2long(str, &val, 10);
    if (r)
        return -1;

    *count = val;

    return 0;
}

long getdents64(unsigned int fd, struct linux_dirent64 *dirent,
                unsigned int count);

long sys_getdents64(struct syscall *call)
{
    long r;
    unsigned int fd;
    struct linux_dirent64 dirent[4096];
    unsigned int count;

    /* First argument.  */
    r = parse_fd(call->args[0], &fd);
    if (r)
        return -1;

    /* Second argument.  */
    r = parse_dirent(call->args[1], dirent);
    if (r)
        return -1;

    /* Third argument.  */
    r = parse_count(call->args[2], &count);
    if (r)
        return -1;

    /* Guard against memory corruption.  */
    if (sizeof(dirent) < count)
        return -1;

    /* System call.  */
    r = getdents64(fd, dirent, count);

    /* Special case.  */
    r = call->exitcode;

    call->success = 1;
    return r;
}
