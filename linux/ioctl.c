/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <types.h>

static const char *parse_command(const char *str, unsigned int *cmd)
{
    const char *s = str;

    while (1) {
        if (strncmp("SNDCTL_TMR_TIMEBASEorTCGETS", s, 27) == 0) {
            *cmd = TCGETS;
            s = s + 27;
        } else if (strncmp("TCGETS", s, 6) == 0) {
            *cmd = TCGETS;
            s = s + 6;
        } else if (strncmp("TIOCGWINSZ", s, 10) == 0) {
            *cmd = TIOCGWINSZ;
            s = s + 10;
        } else if (s[0] == ' ') {
            s++;
        } else if (s[0] == 0) {
            break;
        } else {
            return s;
        }
    }

    return 0;
}

long ioctl(unsigned int fd, unsigned int cmd, unsigned long arg);

long sys_ioctl(struct syscall *call)
{
    long r;
    const char *context;
    unsigned int fd, cmd = 0;
    int arg[1024];

    /* First argument.  */
    r = parse_fd(call->args[0], &fd);
    if (r)
        return -1;

    /* Second argument.  */
    context = parse_command(call->args[1], &cmd);
    if (context) {
        call->context = context;
        return -1;
    }

    /* Third argument.  */
    memset(&arg, 0, sizeof(arg));

    /* System call.  */
    r = ioctl(fd, cmd, (unsigned long) &arg);

    call->success = 1;
    return r;
}
