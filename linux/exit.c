/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <linux/types.h>

int write(int fd, const void *buf, int count);

void _exit(int status);

long sys_write(const struct syscall *call)
{
    int r;
    long fd, count;
    const void *buf;

    r = str2long(call->args[0], &fd, 10);
    if (r)
        return ARGERR;

    buf = call->args[1];

    r = str2long(call->args[2], &count, 10);
    if (r)
        return ARGERR;

    r = write(fd, buf, count);

    return r;
}
