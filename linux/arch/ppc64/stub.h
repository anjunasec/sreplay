/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#define LOADARGS1     std r3, 80(r1)
#define LOADARGS2     std r4, 88(r1); LOADARGS1
#define LOADARGS3     std r5, 96(r1); LOADARGS2
#define LOADARGS4     trap; LOADARGS3
#define LOADARGS5     trap; LOADARGS4
#define LOADARGS6     trap; LOADARGS5

#define ALIGNARG(log2) log2

# define OPD_ENT(name)	.quad BODY_LABEL (name), .TOC.@tocbase, 0

#define ENTRY_1(name)	\
	.section	".text";		\
	.type BODY_LABEL(name),@function;	\
	.globl name;				\
	.section ".opd","aw";			\
	.align 3;				\
name##: OPD_ENT (name);				\
	.previous;

# define BODY_LABEL(X) .##X
# define ENTRY_2(name)	\
	.globl BODY_LABEL(name);		\
	ENTRY_1(name)				\
	.size name, 24;
# define END_2(name)	\
	.size BODY_LABEL(name),.-BODY_LABEL(name);

#define ENTRY(name)	                        \
	ENTRY_2(name)				\
	.align ALIGNARG(2);			\
BODY_LABEL(name):

#define END(name) \
  END_2(name)

#define SYSCALL(name,code,args) \
        ENTRY(name)             \
        li r0, code;            \
        sc;                     \
        mfcr r0;                \
        rldicl. r9,r0,36,63;    \
        beq+ 1f;                \
        mulli r3, r3, -1;       \
     1: blr;                    \
        END(name)

#define SYSCALL6(name,code,args) SYSCALL(name,code,args)
