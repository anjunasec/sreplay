/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#ifndef TYPES_H
#define TYPES_H

typedef unsigned long long u64;
typedef long long s64;

typedef long int off_t;
typedef unsigned int size_t;

typedef void __signalfn_t(int);
typedef __signalfn_t *__sighandler_t;

typedef void __restorefn_t(void);
typedef __restorefn_t *__sigrestore_t;

#define _NSIG_BPW 64

typedef struct {
    unsigned long long sig[1];
} sigset_t;

struct sigaction {
    __sighandler_t sa_handler;
    unsigned long sa_flags;
    __sigrestore_t sa_restorer;
    sigset_t sa_mask;
};

#define SA_NOCLDSTOP	0x00000001U
#define SA_NOCLDWAIT	0x00000002U
#define SA_SIGINFO	0x00000004U
#define SA_ONSTACK	0x08000000U
#define SA_RESTART	0x10000000U
#define SA_NODEFER	0x40000000U
#define SA_RESETHAND	0x80000000U
#define SA_RESTORER     0x04000000

#define TCGETS     0x402c7413
#define TIOCGWINSZ 0x40087468

#define O_DIRECTORY   040000
#define O_NOFOLLOW   0100000
#define O_LARGEFILE  0200000
#define O_DIRECT     0400000

#define FUTEX_WAKE		1

typedef unsigned int u32;

typedef long time_t;

struct timespec {
    time_t tv_sec;
    long tv_nsec;
};

struct stat64 {
    unsigned long long st_dev;  /* Device.  */
    unsigned long long st_ino;  /* File serial number.  */
    unsigned int st_mode;       /* File mode.  */
    unsigned int st_nlink;      /* Link count.  */
    unsigned int st_uid;        /* User ID of the file's owner.  */
    unsigned int st_gid;        /* Group ID of the file's group. */
    unsigned long long st_rdev; /* Device number, if device.  */
    unsigned short __pad2;
    long long st_size;          /* Size of file, in bytes.  */
    int st_blksize;             /* Optimal block size for I/O.  */
    long long st_blocks;        /* Number 512-byte blocks allocated. */
    int st_atime;               /* Time of last access.  */
    unsigned int st_atime_nsec;
    int st_mtime;               /* Time of last modification.  */
    unsigned int st_mtime_nsec;
    int st_ctime;               /* Time of last status change.  */
    unsigned int st_ctime_nsec;
    unsigned int __unused4;
    unsigned int __unused5;
};

#define _NSIG		64

#define SIGHUP		 1
#define SIGINT		 2
#define SIGQUIT		 3
#define SIGILL		 4
#define SIGTRAP		 5
#define SIGABRT		 6
#define SIGIOT		 6
#define SIGBUS		 7
#define SIGFPE		 8
#define SIGKILL		 9
#define SIGUSR1		10
#define SIGSEGV		11
#define SIGUSR2		12
#define SIGPIPE		13
#define SIGALRM		14
#define SIGTERM		15
#define SIGSTKFLT	16
#define SIGCHLD		17
#define SIGCONT		18
#define SIGSTOP		19
#define SIGTSTP		20
#define SIGTTIN		21
#define SIGTTOU		22
#define SIGURG		23
#define SIGXCPU		24
#define SIGXFSZ		25
#define SIGVTALRM	26
#define SIGPROF		27
#define SIGWINCH	28
#define SIGIO		29
#define SIGPOLL		SIGIO
#define SIGPWR		30
#define SIGSYS		31
#define	SIGUNUSED	31
#define SIGRTMIN	32
#define SIGRT_1  33
#define SIGRT_2  34
#define SIGRTMAX	_NSIG

#define SIG_DFL	( (__sighandler_t)0)    /* default signal handling */
#define SIG_IGN	( (__sighandler_t)1)    /* ignore signal */
#define SIG_ERR	( (__sighandler_t)-1)   /* error return from signal */

struct timeval {
    time_t tv_sec;              /* seconds */
    long tv_usec;               /* microseconds */
};

struct timezone {
    int tz_minuteswest;         /* minutes west of Greenwich */
    int tz_dsttime;             /* type of dst correction */
};

typedef long unsigned int ino_t;
typedef long unsigned int nlink_t;
typedef unsigned int uid_t;
typedef unsigned int gid_t;
typedef unsigned int mode_t;

struct __old_kernel_stat {
    unsigned long   st_dev;
    ino_t       st_ino;
    nlink_t     st_nlink;
    mode_t      st_mode;
    uid_t       st_uid;
    gid_t       st_gid;
    unsigned long   st_rdev;
    off_t       st_size;
    unsigned long   st_blksize;
    unsigned long   st_blocks;
    unsigned long   st_atime;
    unsigned long   st_atime_nsec;
    unsigned long   st_mtime;
    unsigned long   st_mtime_nsec;
    unsigned long   st_ctime;
    unsigned long   st_ctime_nsec;
    unsigned long   __unused4;
    unsigned long   __unused5;
    unsigned long   __unused6;
};

#endif
