/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#ifndef TYPES_H
#define TYPES_H

typedef unsigned long long u64;
typedef long long s64;

typedef long int off_t;
typedef unsigned int size_t;

typedef void __signalfn_t(int);
typedef __signalfn_t *__sighandler_t;

typedef void __restorefn_t(void);
typedef __restorefn_t *__sigrestore_t;

#define _NSIG_BPW 32

typedef struct {
    unsigned long sig[2];
} sigset_t;

struct sigaction {
    __sighandler_t sa_handler;
    unsigned long sa_flags;
    __sigrestore_t sa_restorer;
    sigset_t sa_mask;
};

#define SA_NOCLDSTOP	0x00000001U
#define SA_NOCLDWAIT	0x00000002U
#define SA_SIGINFO	0x00000004U
#define SA_ONSTACK	0x08000000U
#define SA_RESTART	0x10000000U
#define SA_NODEFER	0x40000000U
#define SA_RESETHAND	0x80000000U
#define SA_RESTORER     0x04000000

#define TCGETS     0x5401
#define TIOCGWINSZ 0x5413

#define O_LARGEFILE	00100000
#define O_DIRECTORY	00200000

typedef unsigned int u32;

typedef long time_t;

struct timespec {
    time_t tv_sec;
    long tv_nsec;
};

#define FUTEX_WAKE		1

struct stat64 {
    unsigned long long st_dev;
    unsigned char __pad0[4];
    unsigned long __st_ino;
    unsigned int st_mode;
    unsigned int st_nlink;
    unsigned long st_uid;
    unsigned long st_gid;
    unsigned long long st_rdev;
    unsigned char __pad3[4];
    long long st_size;
    unsigned long st_blksize;
    unsigned long long st_blocks;
    unsigned long st_atime;
    unsigned long st_atime_nsec;
    unsigned long st_mtime;
    unsigned int st_mtime_nsec;
    unsigned long st_ctime;
    unsigned long st_ctime_nsec;
    unsigned long long st_ino;
};

#define SIG_DFL	( (__sighandler_t)0)    /* default signal handling */
#define SIG_IGN	( (__sighandler_t)1)    /* ignore signal */
#define SIG_ERR	( (__sighandler_t)-1)   /* error return from signal */

#define SIGHUP    1
#define SIGINT    2
#define SIGKILL   9
#define SIGPIPE  13
#define SIGTERM  15
#define SIGCHLD  17
#define SIGSTOP  19
#define SIGRTMIN 32
#define SIGRT_1  33
#define SIGRT_2  34

struct timeval {
    time_t tv_sec;              /* seconds */
    long tv_usec;               /* microseconds */
};

struct timezone {
    int tz_minuteswest;         /* minutes west of Greenwich */
    int tz_dsttime;             /* type of dst correction */
};

struct __old_kernel_stat {
    unsigned short st_dev;
    unsigned short st_ino;
    unsigned short st_mode;
    unsigned short st_nlink;
    unsigned short st_uid;
    unsigned short st_gid;
    unsigned short st_rdev;
    unsigned long  st_size;
    unsigned long  st_atime;
    unsigned long  st_mtime;
    unsigned long  st_ctime;
};

#endif
