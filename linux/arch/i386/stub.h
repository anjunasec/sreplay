/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#define PUSHARGS0
#define PUSHARGS1 pushl %ebx
#define PUSHARGS2 PUSHARGS1
#define PUSHARGS3 PUSHARGS1
#define PUSHARGS4 pushl %esi; PUSHARGS3
#define PUSHARGS5 pushl %edi; PUSHARGS4
#define PUSHARGS6 pushl %ebp; PUSHARGS5

#define LOADARGS0
#define LOADARGS1 movl 8(%esp), %ebx
#define	LOADARGS2 movl 12(%esp), %ecx; LOADARGS1
#define	LOADARGS3 movl 16(%esp), %edx; LOADARGS2

/* Note that our offset from the stack pointer changes with a push.  */
#define	LOADARGS4 movl 0xc(%esp), %ebx;  \
                  movl 0x10(%esp), %ecx; \
                  movl 0x14(%esp), %edx; \
                  movl 0x18(%esp), %esi

#define	LOADARGS6 movl 0x14(%esp), %ebx; \
                  movl 0x18(%esp), %ecx; \
                  movl 0x1c(%esp), %edx; \
                  movl 0x20(%esp), %esi; \
                  movl 0x24(%esp), %edi; \
                  movl 0x28(%esp), %ebp

#define LOADSCALL(n) mov $n, %eax

#define ENTERKERN int $0x80

#define POPARGS0
#define POPARGS1 popl %ebx
#define POPARGS2 POPARGS1
#define POPARGS3 POPARGS1
#define POPARGS4 POPARGS3; popl %esi
#define POPARGS5 POPARGS4; popl %edi
#define POPARGS6 POPARGS5; popl %ebp

#define RETURN   ret

#define SYSCALL(name,code,args) \
.globl name;	                \
name:	                        \
    PUSHARGS##args;             \
    LOADARGS##args;             \
    LOADSCALL(code);            \
    ENTERKERN;                  \
    POPARGS##args;              \
    RETURN

/* Used by mmap, which is an old-style syscall.  */
#define SYSCALL6(name,code,args) \
.globl name;                     \
name:                            \
	movl %ebx, %edx;         \
	LOADSCALL(code);         \
	lea 4(%esp), %ebx;       \
	ENTERKERN;               \
	movl %edx, %ebx;         \
	RETURN
