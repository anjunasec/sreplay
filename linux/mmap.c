/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <types.h>

#define PROT_READ	0x1     /* Page can be read.  */
#define PROT_WRITE	0x2     /* Page can be written.  */
#define PROT_EXEC	0x4     /* Page can be executed.  */
#define PROT_NONE	0x0     /* Page can not be accessed.  */

static int mmap_prot(const char *str, unsigned long *prot)
{
    const char *s = str;
    *prot = 0x0;

    while (1) {
        if (strncmp("PROT_READ", s, 9) == 0) {
            *prot |= (long) PROT_READ;
            s = s + 9;
        } else if (strncmp("PROT_WRITE", s, 10) == 0) {
            *prot |= (long) PROT_WRITE;
            s = s + 10;
        } else if (strncmp("PROT_EXEC", s, 9) == 0) {
            *prot |= (long) PROT_EXEC;
            s = s + 9;
        } else if (strncmp("PROT_NONE", s, 9) == 0) {
            *prot |= (long) PROT_NONE;
            s = s + 9;
        } else {
            return -1;
        }
        if (s[0] == '|')
            s++;
        if (s[0] == 0)
            break;
    }

    return 0;
}

#define MAP_SHARED	0x01    /* Share changes.  */
#define MAP_PRIVATE	0x02    /* Changes are private.  */

#define MAP_FIXED	0x10    /* Interpret addr exactly.  */
#define MAP_FILE	0
#define MAP_ANONYMOUS	0x20    /* Don't use a file.  */
#define MAP_ANON	MAP_ANONYMOUS

#define MAP_DENYWRITE	0x0800  /* ETXTBSY */
#define MAP_FOOBAR	0x0800  /* ETXTBSY */

static long mmap_flags(const char *str, unsigned long *flags)
{
    const char *s = str;
    *flags = 0x0;

    while (1) {
        if (strncmp("MAP_SHARED", s, 10) == 0) {
            *flags |= (long) MAP_SHARED;
            s = s + 10;
        } else if (strncmp("MAP_PRIVATE", s, 11) == 0) {
            *flags |= (long) MAP_PRIVATE;
            s = s + 11;
        } else if (strncmp("MAP_FIXED", s, 9) == 0) {
            *flags |= (long) MAP_FIXED;
            s = s + 9;
        } else if (strncmp("MAP_FILE", s, 8) == 0) {
            *flags |= (long) MAP_FILE;
            s = s + 8;
        } else if (strncmp("MAP_ANONYMOUS", s, 13) == 0) {
            *flags |= (long) MAP_ANONYMOUS;
            s = s + 13;
        } else if (strncmp("MAP_DENYWRITE", s, 13) == 0) {
            *flags |= (long) MAP_DENYWRITE;
            s = s + 13;
        } else {
            return -1;
        }
        if (s[0] == '|')
            s++;
        if (s[0] == 0)
            break;
    }

    return 0;
}

/* Linux/i386 didn't use to be able to handle more than 4 system call
   parameters, so some system calls used a memory block for parameter
   passing. */
struct mmap_arg_struct {
    unsigned long addr;
    unsigned long len;
    unsigned long prot;
    unsigned long flags;
    unsigned long fd;
    unsigned long offset;
};

int mmap(struct mmap_arg_struct arg);

long sys_mmap(struct syscall *call)
{
    void *start, *retval;
    struct mmap_arg_struct arg;
    long r, offset;
    unsigned long prot, flags;
    size_t length;
    int fd;

    r = str2ptr(call->args[0], &start);
    if (r)
        return ARGERR;

    /* Special case.  */
    if (start == 0x0) {
        str2ptr(call->retval, &retval);
        if (r)
            return -1;
        start = retval;
    }

    r = mmap_prot(call->args[2], &prot);
    if (r)
        return ARGERR;

    length = atol(call->args[1]);

    r = mmap_flags(call->args[3], &flags);
    if (r)
        return ARGERR;

    fd = atol(call->args[4]);

    r = str2long(call->args[5], &offset, 16);
    if (r)
        return ARGERR;

    arg.addr = (long) start;
    arg.len = length;
    arg.prot = prot;
    arg.flags = flags;
    arg.fd = fd;
    arg.offset = offset;

    r = mmap(arg);

    call->success = 1;
    return r;
}

long mmap2(unsigned long addr, unsigned long len, unsigned long prot,
           unsigned long flags, unsigned long fd, unsigned long pgoff);

long sys_mmap2(struct syscall *call)
{
    void *addr, *retval;
    unsigned long len, prot, flags, fd, pgoff;
    long r;

    r = str2ptr(call->args[0], &addr);
    if (r)
        return ARGERR;

    /* Special case.  */
    if (addr == 0x0) {
        r = str2ptr(call->retval, &retval);
        if (r)
            return -1;
        addr = retval;
    }

    r = mmap_prot(call->args[2], &prot);
    if (r)
        return ARGERR;

    len = atol(call->args[1]);

    r = mmap_flags(call->args[3], &flags);
    if (r)
        return ARGERR;

    fd = atol(call->args[4]);

    pgoff = atol(call->args[5]);

    r = mmap2((unsigned long) addr, len, prot, flags, fd, pgoff);

    call->success = 1;
    return r;
}

int mprotect(const void *addr, size_t len, int prot);

long sys_mprotect(struct syscall *call)
{
    void *addr;
    unsigned long prot;
    long len;
    long r;

    r = str2ptr(call->args[0], &addr);
    if (r)
        return ARGERR;

    r = str2long(call->args[1], &len, 10);
    if (r)
        return ARGERR;

    r = mmap_prot(call->args[2], &prot);
    if (r)
        return ARGERR;

    r = mprotect(addr, len, prot);

    call->success = 1;
    return r;
}
