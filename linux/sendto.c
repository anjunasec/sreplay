/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <types.h>

typedef unsigned short sa_family_t;

struct sockaddr {
    sa_family_t sa_family;
    char sa_data[14];
};

#define MSG_DONTWAIT	0x40
#define MSG_EOR         0x80
#define MSG_FIN         0x200
#define MSG_SYN		0x400
#define MSG_RST		0x1000
#define MSG_NOSIGNAL	0x4000
#define MSG_ERRQUEUE	0x2000
#define MSG_MORE	0x8000

static const char *parse_flags(const char *str, unsigned int *flags)
{
    int r;
    void *ptr;
    unsigned long flag;
    const char *s = str;
    *flags = 0x0;

    while (1) {
        if (strncmp("MSG_EOR", s, 7) == 0) {
            *flags |= MSG_EOR;
            s = s + 7;
        } else if (strncmp("MSG_DONTWAIT", s, 12) == 0) {
            *flags |= MSG_DONTWAIT;
            s = s + 12;
        } else if (strncmp("MSG_FIN", s, 7) == 0) {
            *flags |= MSG_FIN;
            s = s + 7;
        } else if (strncmp("MSG_SYN", s, 7) == 0) {
            *flags |= MSG_SYN;
            s = s + 7;
        } else if (strncmp("MSG_RST", s, 7) == 0) {
            *flags |= MSG_RST;
            s = s + 7;
        } else if (strncmp("MSG_NOSIGNAL", s, 12) == 0) {
            *flags |= MSG_NOSIGNAL;
            s = s + 12;
        } else if (strncmp("MSG_ERRQUEUE", s, 12) == 0) {
            *flags |= MSG_ERRQUEUE;
            s = s + 12;
        } else if (strncmp("MSG_MORE", s, 8) == 0) {
            *flags |= MSG_MORE;
            s = s + 8;
        } else if (s[0] == '0' && s[1] == 'x') {
            r = str2ptr(s, &ptr);
            if (r)
                return s;
            flag = (unsigned long) ptr;
            *flags |= flag;
            while (s[0] != 0 && s[0] != '|')
                s++;
        } else if (s[0] == '|') {
            s++;
        } else if (s[0] == 0) {
            break;
        } else {
            return s;
        }
    }

    return 0;
}

#define AF_DECnet	12

static const char *parse_sockaddr(const char *str, struct sockaddr *to)
{
    int i, lim;
    const char *s = str;

    /* Zero struct first.  */
    memset(to, 0, sizeof(struct sockaddr));

    /* Opening brace in struct.  */
    if (*s++ != '{')
        return s;

    /* Get the value of the first field.  */
    if (strncmp("sa_family=AF_DECnet", s, 19) == 0) {
        to->sa_family = AF_DECnet;
        s = s + 19;
    } else {
        return s;
    }

    /* A comma seperating the fields.  */
    if (s[0] == ',')
        s++;

    /* Consume whitespace between fields.  */
    while (*s && s[0] == ' ')
        s++;

    /* Copy the contents of the field.  */
    if (strncmp("sa_data=", s, 8) == 0) {
        s = s + 8;
        lim = sizeof(to->sa_data);
        for (i = 0; i < lim; i++, s++) {
            to->sa_data[i] = s[0];
        }
    } else {
        return s;
    }

    /* Closing brace in struct.  */
    if (*s++ != '}')
        return s;

    return 0;
}

long sendto(int fd, void *buff, size_t len,
            unsigned flags, struct sockaddr *addr, int addr_len);

long socketcall(int call, unsigned long *args);

#define SYS_SENDTO 11

long sys_sendto(struct syscall *call)
{
    int r;
    const char *context;

    long fd;
    void *buff;
    long len;
    unsigned int flags;
    struct sockaddr addr;
    long addr_len;

    unsigned long a[6];

    /* Parse first argument.  */
    r = str2long(call->args[0], &fd, 10);
    if (r)
        return -1;

    /* Parse second argument.  */
    r = str2ptr(call->args[1], &buff);
    if (r)
        return -1;

    /* Parse third argument.  */
    r = str2long(call->args[2], &len, 10);
    if (r)
        return -1;

    /* Parse fourth argument.  */
    context = parse_flags(call->args[3], &flags);
    if (context) {
        call->context = context;
        return -1;
    }

    /* Parse fifth argument.  */
    context = parse_sockaddr(call->args[4], &addr);
    if (context) {
        call->context = context;
        return -1;
    }

    /* Parse sixth argument.  */
    r = str2long(call->args[5], &addr_len, 10);
    if (r) {
        call->context = call->args[0];
        return -1;
    }

    /* Fill out the argument structure.  */
    a[0] = fd;
    a[1] = (unsigned long) buff;
    a[2] = len;
    a[3] = flags;
    a[4] = (unsigned long) &addr;
    a[5] = addr_len;

    /* Fire off the actual system call now.  */
    r = socketcall(SYS_SENDTO, a);

    call->success = 1;
    return r;
}
