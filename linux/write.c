/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <types.h>

int write(unsigned int fd, const char *buf, size_t count);

long sys_write(struct syscall *call)
{
    int r;
    unsigned int fd;
    char *buf;
    size_t count;

    /* First argument.  */
    r = parse_fd(call->args[0], &fd);
    if (r)
        return -1;

    /* Second argument.  */
    buf = call->args[1];

    /* Third argument.  */
    r = str2uint(call->args[2], &count, 10);
    if (r)
        return -1;

    /* Special case. */
    r = strlen(buf);
    if (buf[r - 3] == '.' && buf[r - 2] == '.' && buf[r - 1] == '.') {
        buf[r] = '\n';
        buf[r + 1] = 0;
    }

    /* Special case.  */
    count = strlen(buf);

    /* System call.  */
    r = write(fd, buf, count);

    /* Special case.  */
    r = call->exitcode;

    call->success = 1;
    return r;
}
