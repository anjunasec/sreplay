/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <types.h>

#define O_RDONLY 00000000
#define O_WRONLY 00000001
#define O_RDWR   00000002

#define O_CREAT    00000100
#define O_TRUNC    00001000
#define O_APPEND   00002000
#define O_NONBLOCK 00004000
#define O_EXCL     00000200
#define O_CLOEXEC  02000000

static const char *open_flags(const char *str, int *flags)
{
    const char *s = str;
    *flags = 0x0;

    while (1) {
        if (strncmp("O_RDONLY", s, 8) == 0) {
            *flags |= O_RDONLY;
            s = s + 8;
        } else if (strncmp("O_WRONLY", s, 8) == 0) {
            *flags |= O_WRONLY;
            s = s + 8;
        } else if (strncmp("O_RDWR", s, 6) == 0) {
            *flags |= O_RDWR;
            s = s + 6;
        } else if (strncmp("O_CREAT", s, 7) == 0) {
            *flags |= O_CREAT;
            s = s + 7;
        } else if (strncmp("O_TRUNC", s, 7) == 0) {
            *flags |= O_TRUNC;
            s = s + 7;
        } else if (strncmp("O_EXCL", s, 6) == 0) {
            *flags |= O_EXCL;
            s = s + 6;
        } else if (strncmp("O_APPEND", s, 8) == 0) {
            *flags |= O_APPEND;
            s = s + 8;
        } else if (strncmp("O_LARGEFILE", s, 11) == 0) {
            *flags |= O_LARGEFILE;
            s = s + 11;
        } else if (strncmp("O_DIRECTORY", s, 11) == 0) {
            *flags |= O_DIRECTORY;
            s = s + 11;
        } else if (strncmp("O_NONBLOCK", s, 10) == 0) {
            *flags |= O_NONBLOCK;
            s = s + 10;
		} else if (strncmp("O_CLOEXEC", s, 9) == 0) {
			*flags |= O_CLOEXEC;
			s = s + 9;	
        } else {
            return s;
        }
        if (s[0] == '|')
            s++;
        if (s[0] == 0)
            break;
    }

    return 0;
}

int open(char const *pathname, int flags);

long sys_open(struct syscall *call)
{
    char const *pathname;
    char const *context;
    int r, flags;

    /* First argument.  */
    pathname = call->args[0];

    /* Second argument.  */
    context = open_flags(call->args[1], &flags);
    if (context) {
        call->context = context;
        return -1;
    }

    /* Fire off the system call.  */
    r = open(pathname, flags);

    /* Special case for when running under debugger.  */
    if (r > 0 && r != call->exitcode) {
        r = call->exitcode;
    }

    call->success = 1;
    return r;
}
