/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <print.h>
#include <types.h>

int read(int fd, void *buf, size_t count);

long sys_read(struct syscall *call)
{
    int fd, count, r;
    char buf[2 * 4096];

    fd = atol(call->args[0]);
    count = atol(call->args[2]);

    if (sizeof(buf) < count) {
        print("sreplay: read buffer too small");
        return -1;
    }

    r = read(fd, &buf, count);

    call->success = 1;
    return r;
}
