/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>
#include <types.h>

static const char *parse_signal(const char *str, int *sig)
{
    const char *s = str;

    if (strncmp("SIGRTMIN", s, 8) == 0) {
        *sig = SIGRTMIN;
    } else if (strncmp("SIGRT_1", s, 7) == 0) {
        *sig = SIGRT_1;
    } else if (strncmp("SIGRT_2", s, 7) == 0) {
        *sig = SIGRT_2;
    } else if (strncmp("SIGTERM", s, 7) == 0) {
        *sig = SIGTERM;
    } else if (strncmp("SIGKILL", s, 7) == 0) {
        *sig = SIGKILL;
    } else if (strncmp("SIGSTOP", s, 7) == 0) {
        *sig = SIGSTOP;
    } else if (strncmp("SIGINT", s, 6) == 0) {
        *sig = SIGINT;
    } else if (strncmp("SIGHUP", s, 6) == 0) {
        *sig = SIGHUP;
    } else if (strncmp("SIGPIPE", s, 7) == 0) {
        *sig = SIGPIPE;
    } else if (strncmp("SIGCHLD", s, 7) == 0) {
        *sig = SIGCHLD;
    } else {
        return s;
    }

    return 0;
}

static void sa_handler(int x)
{
    return;
}

static int sigaddset(sigset_t * set, int signo)
{
    unsigned long sig = signo - 1;
    unsigned long long unit = 1;
    int index = sig / _NSIG_BPW;
    set->sig[index] |= unit << (sig % _NSIG_BPW);
    return 0;
}

static const char *parse_sigset(const char *str, sigset_t * set)
{
    const char *s = str;

    memset(set, 0, sizeof(sigset_t));

    while (1) {
        if (s[0] == '[') {
            s++;
        } else if (strncmp("RTMIN", s, 5) == 0) {
            sigaddset(set, SIGRTMIN);
            s = s + 5;
        } else if (strncmp("RT_1", s, 4) == 0) {
            sigaddset(set, SIGRT_1);
            s = s + 4;
        } else if (strncmp("TERM", s, 4) == 0) {
            sigaddset(set, SIGTERM);
            s = s + 4;
        } else if (strncmp("KILL", s, 4) == 0) {
            sigaddset(set, SIGKILL);
            s = s + 4;
        } else if (strncmp("STOP", s, 4) == 0) {
            sigaddset(set, SIGSTOP);
            s = s + 4;
        } else if (strncmp("INT", s, 3) == 0) {
            sigaddset(set, SIGINT);
            s = s + 3;
        } else if (strncmp("HUP", s, 3) == 0) {
            sigaddset(set, SIGHUP);
            s = s + 3;
        } else if (strncmp("PIPE", s, 4) == 0) {
            sigaddset(set, SIGPIPE);
            s = s + 4;
        } else if (s[0] == ']') {
            s++;
        } else if (s[0] == ' ') {
            s++;
        } else if (s[0] == ',') {
            break;
        } else if (s[0] == 0) {
            break;
        } else {
            return s;
        }
    }

    return 0;
}

static const char *parse_sigaction(const char *str, struct sigaction *act)
{
    const char *s = str;
    const char *context;
    char buff[32];
    int i, r;
    void *ptr;
    sigset_t set;
    unsigned long flags = 0;

    memset(act, 0, sizeof(act));

    /* Opening brace in sigaction struct.  */
    if (*s++ != '{')
        return s;

    /* We either have a single member or three members.  */
    if (strncmp(s, "SIG_", 4) == 0) {
        if (strncmp(s, "SIG_IGN", 7) == 0) {
            act->sa_handler = SIG_IGN;
            return 0;
        } else if (strncmp(s, "SIG_DFL", 7) == 0) {
            act->sa_handler = SIG_DFL;
            return 0;
        }
        return s;
    }

    /* Followed by the sighandler function pointer.  */
    for (i = 0; *s && *s != ',' && i < sizeof(buff); i++) {
        buff[i] = *s++;
    }
    buff[i] = 0;
    r = str2ptr(buff, &ptr);
    if (r)
        return s;
    act->sa_handler = sa_handler;

    /* Followed by the mask.  */
    while (*s && *s++ != ',');
    context = parse_sigset(s, &set);
    if (context) {
        return context;
    }
    act->sa_mask = set;

    while (*s && *s++ != ',');
    while (*s && *s == ' ')
        s++;

    /* Finally, a flag list.  */
    while (1) {
        if (strncmp("SA_SIGINFO", s, 10) == 0) {
            flags |= SA_SIGINFO;
            s = s + 10;
        } else if (strncmp("SA_RESTART", s, 10) == 0) {
            flags |= SA_RESTART;
            s = s + 10;
        } else if (strncmp("SA_RESTORER", s, 11) == 0) {
            flags |= SA_RESTORER;
            s = s + 11;
        } else if (strncmp("0", s, 1) == 0) {
            if (s[1] == 'x') {
                memset(buff, 0, sizeof(buff));
                for (i = 0; *s && *s != '}'; i++, s++) {
                    buff[i] = *s;
                }
                r = str2ptr(buff, &ptr);
                if (r) {
                    return s - i;
                }
                act->sa_restorer = ptr;
            } else {
                s++;
            }
        } else if (*s == ' ') {
            s++;
        } else if (s[0] == '|') {
            s++;
        } else if (s[0] == '}') {
            break;
        } else if (s[0] == ',') {
            s++;
        } else {
            return s;
        }
    }

    act->sa_flags = flags;

    return 0;
}

static int parse_oldaction(const char *str, struct sigaction **oact)
{
    long r;
    void *ptr;

    r = str2ptr(str, &ptr);
    if (r) {
        if (strcmp(str, "{SIG_DFL}") == 0) {
            return 0;
        } else if (strcmp(str, "{SIG_IGN}") == 0) {
            return 0;
        }
        return -1;
    }

    *oact = ptr;

    return 0;
}

static int parse_sigsetsize(const char *str, size_t * sigsetsize)
{
    long r, tmp;

    r = str2long(str, &tmp, 10);
    if (r)
        return -1;

    *sigsetsize = tmp;

    return 0;
}


long rt_sigaction(int sig, struct sigaction *act,
                  struct sigaction *oact, size_t sigsetsize);

long sys_rt_sigaction(struct syscall *call)
{
    long r;
    int sig = 0;
    struct sigaction act, oact, *actp = &act, *oactp = &oact;
    size_t sigsetsize;
    const char *context;

    /* Parse first argument, which is an int representing the signal.  */
    context = parse_signal(call->args[0], &sig);
    if (context) {
        call->context = context;
        return -1;
    }

    /* Parse second argument, which is a sigaction struct.  */
    context = parse_sigaction(call->args[1], &act);
    if (context) {
        call->context = context;
        return -1;
    }

    /* Parse third argument, which is also a sigaction struct.  */
    memset(&oact, 0, sizeof(oact));
    r = parse_oldaction(call->args[2], &oactp);
    if (r)
        return -1;

    /* Parse fourth argument, which is an int representing sigset size.  */
    r = parse_sigsetsize(call->args[3], &sigsetsize);
    if (r)
        return -1;

    /* Fire off the actual system call now.  */
    r = rt_sigaction(sig, actp, oactp, sigsetsize);

    call->success = 1;
    return r;
}

#define SIG_BLOCK          0    /* for blocking signals */
#define SIG_UNBLOCK        1    /* for unblocking signals */
#define SIG_SETMASK        2    /* for setting the signal mask */

static int parse_how(const char *str, int *how)
{
    const char *s = str;

    if (strncmp("SIG_UNBLOCK", s, 11) == 0) {
        *how = SIG_UNBLOCK;
    } else if (strncmp("SIG_SETMASK", s, 11) == 0) {
        *how = SIG_SETMASK;
    } else if (strncmp("SIG_BLOCK", s, 9) == 0) {
        *how = SIG_BLOCK;
    } else {
        return -1;
    }

    return 0;
}

static int parse_osigset(const char *str, sigset_t ** set)
{
    const char *s = str;
    const char *context;

    if (strncmp("NULL", s, 4) == 0) {
        *set = 0;
        return 0;
    }

    context = parse_sigset(str, *set);
    if (context)
        return -1;

    return 0;
}

long rt_sigprocmask(int how, sigset_t * set, sigset_t * oset,
                    size_t sigsetsize);

long sys_rt_sigprocmask(struct syscall *call)
{
    long r;
    const char *context;
    int how;
    sigset_t set, oset;
    sigset_t *osetp = &oset;
    size_t sigsetsize;

    /* Parse first argument, which is int representing how mask changes.  */
    r = parse_how(call->args[0], &how);
    if (r)
        return -1;

    /* Parse second argument, which is a sigset_t pointer.  */
    context = parse_sigset(call->args[1], &set);
    if (context) {
        call->context = context;
        return -1;
    }

    /* Parse third argument, which is a sigset_t pointer.  */
    r = parse_osigset(call->args[2], &osetp);
    if (r)
        return -1;

    /* Parse fourth argument, which is an int representing sigset size.  */
    r = parse_sigsetsize(call->args[3], &sigsetsize);
    if (r)
        return -1;

    /* Fire off the actual system call now.  */
    r = rt_sigprocmask(how, &set, osetp, sigsetsize);

    call->success = 1;
    return r;
}
