/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>

typedef long long unsigned int __dev_t;
typedef long unsigned int __ino_t;
typedef unsigned int __mode_t;
typedef unsigned int __nlink_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef long int __off_t;
typedef long int __blksize_t;
typedef long int __blkcnt_t;

typedef long int __time_t;

struct timespec {
    __time_t tv_sec;
    long int tv_nsec;
};

struct stat {
    __dev_t st_dev;
    short unsigned int __pad1;
    __ino_t st_ino;
    __mode_t st_mode;
    __nlink_t st_nlink;
    __uid_t st_uid;
    __gid_t st_gid;
    __dev_t st_rdev;
    short unsigned int __pad2;
    __off_t st_size;
    __blksize_t st_blksize;
    __blkcnt_t st_blocks;
    struct timespec st_atim;
    struct timespec st_mtim;
    struct timespec st_ctim;
    long unsigned int __unused4;
    long unsigned int __unused5;
};


int fstat64(int filedes, struct stat *buf);

long sys_fstat64(struct syscall *call)
{
    int fd;
    struct stat buf;
    int r;

    fd = atol(call->args[0]);

    r = fstat64(fd, &buf);

    call->success = 1;
    return r;
}

int fstat(int filedes, struct stat *buf);

long sys_fstat(struct syscall *call)
{
    long fd;
    struct stat buf;
    int r;

    r = str2long(call->args[0], &fd, 10);
    if (r)
        return ARGERR;

    r = fstat(fd, &buf);

    call->success = 1;
    return r;
}
