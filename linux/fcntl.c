/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>

#define F_SETFD 2

#define FD_CLOEXEC 1

static int parse_command(const char *str, unsigned int *cmd)
{
    const char *s = str;

    while (1) {
        if (strncmp("F_SETFD", s, 7) == 0) {
            *cmd = F_SETFD;
            s = s + 7;
        } else if (s[0] == ' ') {
            s++;
        } else if (s[0] == 0) {
            break;
        } else {
            return -1;
        }
    }

    return 0;
}

static int parse_arg(const char *str, unsigned long *arg)
{
    const char *s = str;

    while (1) {
        if (strncmp("FD_CLOEXEC", s, 10) == 0) {
            *arg = FD_CLOEXEC;
            s = s + 10;
        } else if (s[0] == ' ') {
            s++;
        } else if (s[0] == 0) {
            break;
        } else {
            return -1;
        }
    }

    return 0;
}

long fcntl64(unsigned int fd, unsigned int cmd, unsigned long arg);

long sys_fcntl64(struct syscall *call)
{
    long r;
    unsigned int fd;
    unsigned int cmd;
    unsigned long arg;

    /* First argument.  */
    r = parse_fd(call->args[0], &fd);
    if (r)
        return -1;

    /* Second argument.  */
    r = parse_command(call->args[1], &cmd);
    if (r)
        return -1;

    /* Third argument.  */
    r = parse_arg(call->args[2], &arg);
    if (r)
        return -1;

    /* System call.  */
    r = fcntl64(fd, cmd, arg);

    call->success = 1;
    return r;
}

long fcntl(unsigned int fd, unsigned int cmd, unsigned long arg);

long sys_fcntl(struct syscall *call)
{
    long r;
    unsigned int fd;
    unsigned int cmd;
    unsigned long arg;

    /* First argument.  */
    r = parse_fd(call->args[0], &fd);
    if (r)
        return -1;

    /* Second argument.  */
    r = parse_command(call->args[1], &cmd);
    if (r)
        return -1;

    /* Third argument.  */
    r = parse_arg(call->args[2], &arg);
    if (r)
        return -1;

    /* System call.  */
    r = fcntl(fd, cmd, arg);

    call->success = 1;
    return r;
}
