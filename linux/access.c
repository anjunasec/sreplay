/* Copyright (C) 2005, 2006, 2007
 * Amos Waterland <apw@rossby.metr.ou.edu>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <string.h>
#include <sreplay.h>

#define	R_OK	4               /* Test for read permission.  */
#define	W_OK	2               /* Test for write permission.  */
#define	X_OK	1               /* Test for execute permission.  */
#define	F_OK	0               /* Test for existence.  */

static int access_mode(const char *str, int *mode)
{
    const char *s = str;
    *mode = 0x0;

    while (1) {
        if (strncmp("R_OK", s, 4) == 0) {
            *mode |= (long) R_OK;
            s = s + 4;
        } else if (strncmp("W_OK", s, 4) == 0) {
            *mode |= (long) W_OK;
            s = s + 4;
        } else if (strncmp("X_OK", s, 4) == 0) {
            *mode |= (long) X_OK;
            s = s + 4;
        } else if (strncmp("F_OK", s, 4) == 0) {
            *mode |= (long) F_OK;
            s = s + 4;
        } else {
            return -1;
        }
        if (s[0] == '|')
            s++;
        if (s[0] == 0)
            break;
    }

    return 0;
}

int access(char const *__name, int __type);

long sys_access(struct syscall *call)
{
    char const *pathname;
    int mode;
    long r;

    pathname = call->args[0];

    r = access_mode(call->args[1], &mode);
    if (r)
        return -1;

    r = access(pathname, mode);

    call->success = 1;
    return r;
}
