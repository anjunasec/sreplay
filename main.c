/* Copyright (C) 2005-2013  Amos Waterland <apw@debian.org>
 *
 * This file is part of sreplay.
 *
 * The sreplay program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * The sreplay program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the sreplay program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA.
 */

#include <sreplay.h>
#include <print.h>
#include <string.h>

int write(int fd, const void *buf, int count);
void _exit(int status);

static void usage()
{
    char mesg[] = "`sreplay' plays back system call traces\n\n"
        "Usage: sreplay [OPTIONS] TRACE\n"
        " -?, --help      Print this help statement and exit\n"
        "     --version   Print version statement and exit\n"
        " -n, --dry-run   Parse trace but do not execute it\n\n"
        "Note: TRACE format is that of strace output\n\n"
        "Example: strace true 2> log && sreplay $(cat log)\n";

    write(2, mesg, strlen(mesg));
}

static void version()
{
    char mesg[] = "sreplay " VERSION "." PATCHLEVEL "."
        SUBLEVEL EXTRAVERSION "\n";

    write(2, mesg, strlen(mesg));
}

static void argument()
{
    char const mesg[] = "sreplay: unrecognized argument\n";

    write(2, mesg, strlen(mesg));
}

int main(int argc, char *argv[])
{
    int ret;
    int count = argc - 1;
    char **trace = argv + 1;
    int dry = 0;

    if (argc <= 1) {
        usage();
        _exit(1);
    }

    if (argv[1][0] == '-') {
        if (argv[1][1] == '?') {
            usage();
            _exit(1);
        } else if (argv[1][1] == 'n') {
            dry = 1;
            count--;
            trace++;
        } else if (argv[1][1] == '-') {
            if (argv[1][2] == 'h') {
                usage();
                _exit(0);
            } else if (argv[1][2] == 'v') {
                version();
                _exit(1);
            } else if (argv[1][2] == 'd') {
                dry = 1;
                count--;
                trace++;
            } else {
                argument();
                _exit(1);
            }
        } else {
            argument();
            _exit(1);
        }
    }

    ret = replay(count, trace, dry);

    _exit(ret);
    return 1;
}
