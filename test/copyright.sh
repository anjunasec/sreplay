#!/bin/bash

for f in $(find * -type f | egrep -v 'README|NEWS|ChangeLog'); do
    grep -q 'Lesser General' $f || {
        echo " FAIL     $f (does not have a copyright statement)";
	exit 1;
    };
done

